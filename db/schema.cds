using { Currency, managed, Country } from '@sap/cds/common';
namespace com.gavdilabs.challenge;

entity Order {
  key OrderId : UUID;
  ProductId  : UUID;
  EmployeeNumber  : Integer;
  AmountOrdered : Integer;
  OrderDate  : Date;
  Delivered : Boolean;
  Employee : Association to one Employee on Employee.EmployeeNumber = EmployeeNumber;
  Product : Association to one Product on Product.ProductId = ProductId;
}

entity Product {
  key ProductId : UUID;
  ProductName  : String(50);
  ProductImage : String(255);
  Vendor  : String(50);
  ModelNumber : String(50);
  Price  : DecimalFloat;
  Currency  : Currency;
  InventoryStock  : Integer;
  Deprecated : Boolean;
  Image : String(255);
  Orders : Association to many Order on Orders.ProductId = ProductId;
}

entity Employee {
    key EmployeeNumber : Integer;
    EmployeeName : String(150);
    Title : String (60);
    MaximumBudget : DecimalFloat;
    Image: String(254); //I changed this to 254 because it was annoying Mathias, and I like that.
}