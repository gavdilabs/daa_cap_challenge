const cds = require("@sap/cds");

module.exports = srv => {
    const {OrderSet, ProductSet, EmployeeSet} = srv.entities ()

    srv.before ('CREATE', 'OrderSet', async (req) => {
        const order = req.data
        if (!order.AmountOrdered || order.AmountOrdered <= 0)  return req.error (400, 'You must order at least 1 item')
        const productQuery = await cds.run(SELECT.from(ProductSet).where({ProductId : order.ProductId}));
        const product = productQuery[0];
        const employeeQuery = await cds.run(SELECT.from(EmployeeSet).where({EmployeeNumber : order.EmployeeNumber}));
        const employee = employeeQuery[0];
        if (product.Deprecated) return req.error (400, 'Product has been deprecated and is no longer available.')
        if (order.AmountOrdered > product.InventoryStock) return req.error (400, 'Order amount supercedes product stock.')
        if (order.AmountOrdered * product.Price > employee.MaximumBudget) return req.error (400, 'Insufficient budget.')
    })

    srv.before ('UPDATE', 'OrderSet', async (req) => {
        const order = req.data
        if (!order.AmountOrdered || order.AmountOrdered <= 0)  return req.error (400, 'You must order at least 1 item')
        const productQuery = await cds.run(SELECT.from(ProductSet).where({ProductId : order.ProductId}));
        const product = productQuery[0];
        const employeeQuery = await cds.run(SELECT.from(EmployeeSet).where({EmployeeNumber : order.EmployeeNumber}));
        const employee = employeeQuery[0];
        if (product.Deprecated) return req.error (400, 'Product has been deprecated and is no longer available.')
        if (order.AmountOrdered > product.InventoryStock) return req.error (400, 'Order amount supercedes product stock.')
        if (order.AmountOrdered * product.Price > employee.MaximumBudget) return req.error (400, 'Insufficient budget.')
    })

    srv.on('DeprecateProduct', async(req) => {
        const productQuery = await cds.run(SELECT.from(ProductSet).where({ProductId : req.data.ProductId}));
        const product = productQuery[0];
        if (product.Deprecated) return req.error (400, 'Product has already been deprecated.')
        await cds.run(UPDATE(ProductSet,req.data.ProductId).set({Deprecated: true}));
    });
}