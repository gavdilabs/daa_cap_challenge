using com.gavdilabs.challenge as datamodel from '../db/schema';

service CustomService {
    
  entity ProductSet as projection on datamodel.Product;
  entity EmployeeSet as projection on datamodel.Employee;

  @(Capabilities:{
    InsertRestrictions.Insertable: true,
    UpdateRestrictions.Updatable: true,
    DeleteRestrictions.Deletable: false
  })
  entity OrderSet as projection on datamodel.Order;

  action DeprecateProduct(ProductId : UUID);
}